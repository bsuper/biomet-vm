### Assuming that Virtualbox is downloaded:

1. File > Import Applicance > Select OVA image
2. Start
3. cd ~/biomet # might be in biomet folder be default
4. git pull origin master # checks for any updates
5. jupyter notebook # opens iPython notebook
6. navigate to notebook/ and click on any notebook
7. Kernel > Restart and Run all

Virtualbox guest additions is installed already.

To transfer files to and from the virtual machine, google `shared folders virtualbox windows`

The VM is running an Ubuntu-based operating system called Elementary. You can search for Ubuntu based tutorials and they will work perfectly with the OS.